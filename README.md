# Projet de création d'un inventaire en Blazor

[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=BlazorProject&metric=alert_status&token=517f3f0ae8780061da0076f6b3bcc50f24ede668)](https://codefirst.iut.uca.fr/sonar/dashboard?id=BlazorProject)

Ce projet avait pour but de créer un inventaire type minecraft à l'aide du framework Blazor et ce afin de nous faire découvrir ce dernier.

## Lancement du projet

Etant donné que le projet n'est constitué que de la branche `master`, c'est celle-ci que nous allons utiliser.

Une fois le dépôt cloné, il faut lancer les 2 projets qui y sont présent.

Pour cela, lancer la solution avec Visual Studio : `BlazorApp1.sln`

Ensuite, on va définir les projets de démarrage :

    1. Clique droit sur la solution
    2. Définir les projets de démarrage...
![Image du menu clique droit](/docs/images/ProjetDeDémarrage.png "Titre de l'image")
    
    3. On sélectionne : Plusieurs projets de démarrage
    4. On met les deux projet sur : Démarrer


![Image du menu de sélection des projets](/docs/images/ProjetDeDémarrage2.png "Titre de l'image")

    5. On démarre le projet global
    
![Image du boutton de démarrage](/docs/images/Démarrer.png "Titre de l'image")

Auteurs : Lou Valade et Théo Renaud