﻿using BlazorApp1.Components;
using BlazorApp1.Models;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace BlazorApp1.Pages
{
    public partial class Inventory
    {
        [Inject]
        public IDataService DataService { get; set; }

        private int totalItem;
        public List<Item> Items { get; set; } = new List<Item>();

        [Inject]
        public IStringLocalizer<Inventory> Localizer { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());

            StateHasChanged();
        }

        //Pagination
        private async Task OnReadData(DataGridReadDataEventArgs<Item> e)
        {
            if (e.CancellationToken.IsCancellationRequested)//si la requete n'a pas de retour*
            {
                return;
            }
            
            if (!e.CancellationToken.IsCancellationRequested)
            {
                Items = await DataService.List(e.Page, e.PageSize); //met les Items dans la var Items 
                totalItem = await DataService.Count();// compte le nombre d'items qu'il y a au total dans la liste
            }
        }
    }
}
