﻿namespace BlazorApp1.Models
{
    public class Item
    {
        public Item(int id, string displayName, string name, int stackSize, int stack, int maxDurability, List<string> enchantCategories, List<string> repairWith, DateTime createdDate, DateTime? updatedDate, string imageBase64)
        {
            Id = id;
            DisplayName = displayName;
            Name = name;
            StackSize = stackSize;
            Stack = stack;
            MaxDurability = maxDurability;
            EnchantCategories = enchantCategories;
            RepairWith = repairWith;
            CreatedDate = createdDate;
            UpdatedDate = updatedDate;
            ImageBase64 = imageBase64;
        }

        public Item() {}

        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public int StackSize { get; set; }
        public int Stack { get; set; } = 1;
        public int MaxDurability { get; set; }
        public List<string> EnchantCategories { get; set; }
        public List<string> RepairWith { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string ImageBase64 { get; set; }
    }
}
