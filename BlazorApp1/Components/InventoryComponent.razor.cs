﻿using BlazorApp1.Models;
using BlazorApp1.Pages;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.JSInterop;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace BlazorApp1.Components
{
    public partial class InventoryComponent
    {
        public InventoryComponent()
        {
            Actions = new ObservableCollection<CraftingAction>();
            Actions.CollectionChanged += OnActionsCollectionChanged;
            this.InventoryItems = new List<Item> { null, null, null, null, null, null, null, null, null };
        }

        public ObservableCollection<CraftingAction> Actions { get; set; }
        public Item CurrentDragItem { get; set; }

        [Parameter]
        public List<Item> Items { get; set; }

        public List<Item> InventoryItems { get; set; }

        [Inject]
        public IStringLocalizer<Inventory> Localizer { get; set; }
        public int Stack { get; internal set; }

        /// <summary>
        /// Gets or sets the java script runtime.
        /// </summary>
        [Inject]
        internal IJSRuntime JavaScriptRuntime { get; set; }

        private void OnActionsCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            JavaScriptRuntime.InvokeVoidAsync("Crafting.AddActions", e.NewItems);
        }

    }
}
