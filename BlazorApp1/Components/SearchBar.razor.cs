﻿using Microsoft.Extensions.Localization;
using BlazorApp1.Models;
using Microsoft.AspNetCore.Components;

namespace BlazorApp1.Components
{   
    public partial class SearchBar
    {
        [Parameter]
        public List<Item> ListeItems { get; set; }

        public string SearchText { get; set; }
        
        // Non fonctionnel
        // L'idée, étais de prendre la chaine de caractères saisie par l'utilisateur et de la comparer à la liste de tous les items.
        // Dans le cas où il y a une correspondance, on affiche l'élément
        // Sauf que nous n'avons pas trouvé une bonne manière de le faire
        public void Search()
        {
            foreach (var item in ListeItems)
            {
                if (String.Equals(item.DisplayName, ListeItems) == true)
                {
                    return;
                }
            }
        }
    }
}
