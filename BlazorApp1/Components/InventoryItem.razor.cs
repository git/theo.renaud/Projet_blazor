﻿using BlazorApp1.Models;
using Microsoft.AspNetCore.Components;

namespace BlazorApp1.Components
{
    public partial class InventoryItem
    {
        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public InventoryComponent Parent { get; set; }

        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new CraftingAction { Action = "Drag Enter", Item = this.Item, Index = this.Index });
        }

        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new CraftingAction { Action = "Drag Leave", Item = this.Item, Index = this.Index });
        }

        /// Lors du dépôt d'un élément, on vérifie si l'élément est déjà présent dans la case.
        /// Si c'est le cas, et que le nom des deux éléments sont identiques, on ajoute la quantité de l'élément déposé à l'élément présent. Sinon on ne fait rien.
        /// Si la case est vide, on ajoute l'élément déposé à la case.

        internal void OnDrop()
        {
            Item CurrentHoldItem = new Item(Parent.CurrentDragItem.Id, Parent.CurrentDragItem.DisplayName, Parent.CurrentDragItem.Name, Parent.CurrentDragItem.StackSize, Parent.CurrentDragItem.Stack, Parent.CurrentDragItem.MaxDurability, Parent.CurrentDragItem.EnchantCategories, Parent.CurrentDragItem.RepairWith, Parent.CurrentDragItem.CreatedDate, Parent.CurrentDragItem.UpdatedDate, Parent.CurrentDragItem.ImageBase64);
            

            if (NoDrop)
            {
                return;
            }

            if (this.Item != null)
            {
                if (this.Item.Name == CurrentHoldItem.Name)
                {
                    CurrentHoldItem.Stack = CurrentHoldItem.Stack + this.Item.Stack;
                    this.Item = CurrentHoldItem;
                }
                else {
                    return;
                }
            }

            else
            {
                this.Item = CurrentHoldItem;

                Parent.Actions.Add(new CraftingAction { Action = "Drop", Item = this.Item, Index = this.Index });
            }
        }

        private void OnDragStart()
        {
            Parent.CurrentDragItem = new Item(this.Item.Id, this.Item.DisplayName, this.Item.Name, this.Item.StackSize, this.Item.Stack, this.Item.MaxDurability, this.Item.EnchantCategories, this.Item.RepairWith, this.Item.CreatedDate, this.Item.UpdatedDate, this.Item.ImageBase64);

            Parent.Actions.Add(new CraftingAction { Action = "Drag Start", Item = this.Item, Index = this.Index });

            //Si l'item pris dans la main est un item de la liste des items disponibles, on ne le supprime pas.
            //Par contre, si l'item est un item de la liste des items de l'inventaire, on le supprime.
            
            if (!NoDrop)
            {
                this.Item = null;
            }
        }
    }
}
